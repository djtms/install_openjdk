# install OpenJDK on Windows

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)

## About <a name = "about"></a>

This is simple PSH script to download OpenJDK 14 for Windows 10 machines

## Getting Started <a name = "getting_started"></a>

Script will uninstall Oracle JAVA & install OpenJDK 14, unzip it to desired folder (C:\Program Files\OpenJDK\)

### Prerequisites

Powershell 5.1
Windows 10

### Installing

Just run the script

## Usage <a name = "usage"></a>

No parameters - just run the script

PS C:\Scripts>install_openjdk.ps1 
