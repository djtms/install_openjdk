<#
.SYNOPSIS
  Install newest version of OpenJDK
.DESCRIPTION
  Script will install OpenJDK 14, unzip it to desired folder (C:\Program Files\OpenJDK\)
.PARAMETER <Parameter_Name>
    None
.INPUTS
  None
.OUTPUTS
  Log file stored in C:\Windows\Temp\install_openjdk.log
.NOTES
  Version:        1.0
  Author:         Piotr Berent
  Creation Date:  24.03.2020
  Purpose/Change: Initial script development
  
.EXAMPLE
  install_openjdk.ps1
#>

#---------------------------------------------------------[Initialisations]--------------------------------------------------------
#Requires -RunAsAdministrator
#Set Error Action to Silently Continue
$ErrorActionPreference = "SilentlyContinue"

#-----------------------------------------------------------[Functions]------------------------------------------------------------

function Unzip($zipfile, $outdir)
{
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    $archive = [System.IO.Compression.ZipFile]::OpenRead($zipfile)
    foreach ($entry in $archive.Entries)
    {
        $entryTargetFilePath = [System.IO.Path]::Combine($outdir, $entry.FullName)
        $entryDir = [System.IO.Path]::GetDirectoryName($entryTargetFilePath)
        
        #Ensure the directory of the archive entry exists
        if(!(Test-Path $entryDir )){
            New-Item -ItemType Directory -Path $entryDir | Out-Null 
        }
        
        #If the entry is not a directory entry, then extract entry
        if(!$entryTargetFilePath.EndsWith("\")){
            [System.IO.Compression.ZipFileExtensions]::ExtractToFile($entry, $entryTargetFilePath, $true);
        }
    }
}

function ReloadPath {
  $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") +
              ";" +
              [System.Environment]::GetEnvironmentVariable("Path","User")
}

Function Get-DateFormat 
{
    Get-Date -Format "yyyy-MM-dd-HH-mm-ss"
}

Function Write-Log($Type, $Event)
{
    If ($Type -eq "Info")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [INFO] $Event"
        }
        Write-Host "$(Get-DateFormat) [INFO] $Event"
    }

    If ($Type -eq "Succ")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [SUCCESS] $Event"
        }
        Write-Host -ForegroundColor Green "$(Get-DateFormat) [SUCCESS] $Event"
    }

    If ($Type -eq "Err")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [ERROR] $Event"
        }
        Write-Host -ForegroundColor Red -BackgroundColor Black "$(Get-DateFormat) [ERROR] $Event"
    }

    If ($Type -eq "Conf")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$Event"
        }
        Write-Host -ForegroundColor Cyan -Object "$Event"
    }
}
#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Log File Info
$LogPath = "C:\Windows\Temp"

If ($LogPath)
{
    $LogDate = Get-DateFormat
    $LogFile = "install_openjdk_$LogDate.log"
    $Log = "$LogPath\$LogFile"
    $LogT = Test-Path -Path $Log
    If ($LogT)
    {
        Clear-Content -Path $Log
    }
    Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [INFO] Log started"
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------
Write-Log -Type Info -Event "Closing JAVA applications"
Get-Process -Name jucheck | Stop-Process -Force
Get-Process -Name jusched | Stop-Process -Force
Get-Process -Name jawaw | Stop-Process -Force
Get-Process -Name java | Stop-Process -Force
Get-Process -Name javaws | Stop-Process -Force
Write-Log -Type Info -Event "All JAVA apps were closed"
Write-Log -Type Info -Event "Uninstalling Oracle Java"
Get-WmiObject Win32_Product -filter "name like 'Java%' AND vendor like 'Oracle%'" | ForEach-Object { $_.Uninstall() } | Out-Null
$TestOracleJava = Get-WmiObject Win32_Product -filter "name like 'Java%' AND vendor like 'Oracle%'"
If ($TestOracleJava -ne $Null)
  {
    Write-Log -Type Err -Event "Oracle Java is still installed - check Control Panel"
  }
else {
  Write-Log -Type Succ -Event "Oracle Java unsintalled"
}  
Write-Log -Type Info -Event "Downloading newest stable OpenJDK"
$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile("https://download.java.net/java/GA/jdk14.0.1/664493ef4a6946b186ff29eb326336a2/7/GPL/openjdk-14.0.1_windows-x64_bin.zip","$ENV:TEMP\openjdk14.zip")
Write-Log -Type Info -Event "Downloaded newest stable OpenJDK"
New-Item -ItemType Directory -Path "$ENV:ProgramFiles\openjdk14" | Out-Null
Write-Log -Type Info -Event "Installing newest stable OpenJDK"
Unzip -zipfile "$ENV:TEMP\openjdk14.zip" -outdir "$ENV:ProgramFiles\openjdk14"
$javaexefile = get-childitem -path 'C:\Program Files\openjdk14' -Include *.exe -File -Recurse | Where-Object { $_.Name -eq 'java.exe' }
$javapath = ($javaexefile.DirectoryName).ToString()
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$javapath", "Machine")
ReloadPath
$javatest = "java -version" 
if ($javatest -eq $null) {
  Write-Log -Type Err -Event "No Java detected"
} 
else { 
  Write-Log -Type Succ -Event "Java is installed" 
}
If ($LogPath)
{
    Add-Content -Path $Log -Encoding ASCII -Value "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") [INFO] Log finished"
}